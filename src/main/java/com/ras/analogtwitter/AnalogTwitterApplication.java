package com.ras.analogtwitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalogTwitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnalogTwitterApplication.class, args);
	}

}
