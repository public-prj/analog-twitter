package com.ras.analogtwitter.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;

@Entity
@Table(name = "subscription")
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    @JsonIgnoreProperties(value = "subscriptions", allowSetters = true)
    private User subscriber;

    @ManyToOne
    @JsonIgnoreProperties(value = "subscriptions", allowSetters = true)
    private User subscribee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(User subscriber) {
        this.subscriber = subscriber;
    }

    public User getSubscribee() {
        return subscribee;
    }

    public void setSubscribee(User subscribee) {
        this.subscribee = subscribee;
    }
}
