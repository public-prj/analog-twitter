package com.ras.analogtwitter.service;

import com.ras.analogtwitter.service.dto.SubscriptionDTO;

public interface SubscriptionService {
    SubscriptionDTO save(SubscriptionDTO subscriptionDTO);

    void delete(Long id);

    void unsubscribe(SubscriptionDTO subscriptionDTO) throws Exception;
}
