package com.ras.analogtwitter.service.dto;

import javax.validation.constraints.NotNull;

public class CommentDTO {
    private Long id;

    @NotNull
    private String text;

    private Long postId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }
}
