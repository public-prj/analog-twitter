package com.ras.analogtwitter.service.dto;

public class SubscriptionDTO {

    private Long id;
    private Long subscriberId;
    private Long subscribeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(Long subscriberId) {
        this.subscriberId = subscriberId;
    }

    public Long getSubscribeeId() {
        return subscribeeId;
    }

    public void setSubscribeeId(Long subscribeeId) {
        this.subscribeeId = subscribeeId;
    }
}
