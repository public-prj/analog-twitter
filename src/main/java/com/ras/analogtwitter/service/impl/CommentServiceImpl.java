package com.ras.analogtwitter.service.impl;

import com.ras.analogtwitter.model.Comment;
import com.ras.analogtwitter.repository.CommentRepository;
import com.ras.analogtwitter.service.CommentService;
import com.ras.analogtwitter.service.dto.CommentDTO;
import com.ras.analogtwitter.service.mapper.CommentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    private final Logger log = LoggerFactory.getLogger(CommentServiceImpl.class);

    private final CommentMapper commentMapper;
    private final CommentRepository commentRepository;

    public CommentServiceImpl(CommentMapper commentMapper, CommentRepository commentRepository) {
        this.commentMapper = commentMapper;
        this.commentRepository = commentRepository;
    }

    @Override
    public CommentDTO save(CommentDTO commentDTO) {
        log.debug("Request to save Comment : {}", commentDTO);
        Comment comment = commentMapper.toEntity(commentDTO);
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    @Override
    public CommentDTO update(CommentDTO commentDTO) {
        return null;
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete comment : {}", id);
        commentRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CommentDTO> findAll() {

        log.debug("Request to get all comments");
        return commentRepository.findAll().stream()
                .map(commentMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CommentDTO> findOne(Long id) {
        log.debug("Request to get Comment by id : {}", id);
        return commentRepository.findById(id)
                .map(commentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CommentDTO> getCommentsByPostId(Long postId) {
        log.debug("Request to get Comment by postId : {}", postId);
        return commentRepository.findByPostId(postId).stream()
                .map(commentMapper::toDto).collect(Collectors.toList());
    }
}
