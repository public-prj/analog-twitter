package com.ras.analogtwitter.service.impl;

import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.repository.PostRepository;
import com.ras.analogtwitter.service.PostService;
import com.ras.analogtwitter.service.dto.PostDTO;
import com.ras.analogtwitter.service.mapper.PostMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostServiceImpl implements PostService {
    private final Logger log = LoggerFactory.getLogger(PostServiceImpl.class);
    private final PostMapper postMapper;
    private final PostRepository postRepository;

    public PostServiceImpl(PostMapper postMapper, PostRepository postRepository) {
        this.postMapper = postMapper;
        this.postRepository = postRepository;
    }

    @Override
    public PostDTO save(PostDTO postDTO) {
        log.debug("Request to save Post : {}", postDTO);
        Post post = postMapper.toEntity(postDTO);
        post = postRepository.save(post);
        return postMapper.toDto(post);
    }

    @Override
    public PostDTO update(PostDTO postDTO) {
        return null;
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete post : {}", id);
        postRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PostDTO> findAll() {
        log.debug("Request to get all posts");
        return postRepository.findAll().stream()
                .map(postMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PostDTO> findOne(Long id) {
        log.debug("Request to get Post by id : {}", id);
        return postRepository.findById(id)
                .map(postMapper::toDto);
    }

}
