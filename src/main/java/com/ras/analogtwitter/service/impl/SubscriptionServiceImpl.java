package com.ras.analogtwitter.service.impl;

import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.model.Subscription;
import com.ras.analogtwitter.repository.PostRepository;
import com.ras.analogtwitter.repository.SubscriptionRepository;
import com.ras.analogtwitter.service.SubscriptionService;
import com.ras.analogtwitter.service.dto.SubscriptionDTO;
import com.ras.analogtwitter.service.mapper.PostMapper;
import com.ras.analogtwitter.service.mapper.SubscriptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {
    private final Logger log = LoggerFactory.getLogger(SubscriptionServiceImpl.class);
    private final SubscriptionMapper subscriptionMapper;
    private final SubscriptionRepository subscriptionRepository;

    public SubscriptionServiceImpl(SubscriptionMapper subscriptionMapper, SubscriptionRepository subscriptionRepository) {
        this.subscriptionMapper = subscriptionMapper;
        this.subscriptionRepository = subscriptionRepository;
    }

    @Override
    public SubscriptionDTO save(SubscriptionDTO subscriptionDTO) {
        log.debug("Request to save Subscription : {}", subscriptionDTO);
        Subscription subscription = subscriptionMapper.toEntity(subscriptionDTO);
        subscription = subscriptionRepository.save(subscription);
        return subscriptionMapper.toDto(subscription);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete subscription : {}", id);
        subscriptionRepository.deleteById(id);
    }

    @Override
    public void unsubscribe(SubscriptionDTO subscriptionDTO) throws Exception {
        Subscription subscription = subscriptionRepository.findBySubscriberIdAndSubscribeeId(subscriptionDTO.getSubscriberId(), subscriptionDTO.getSubscribeeId()).orElseThrow(() ->
                new Exception("Subscription not found"));
        this.delete(subscription.getId());
    }


}
