package com.ras.analogtwitter.service.impl;

import com.ras.analogtwitter.model.Like;
import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.repository.LikeRepository;
import com.ras.analogtwitter.service.LikeService;
import com.ras.analogtwitter.service.dto.LikeDTO;
import com.ras.analogtwitter.service.mapper.LikeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LikeServiceImpl implements LikeService {

    private final Logger log = LoggerFactory.getLogger(LikeServiceImpl.class);

    private final LikeMapper likeMapper;
    private final LikeRepository likeRepository;


    public LikeServiceImpl(LikeMapper likeMapper, LikeRepository likeRepository) {
        this.likeMapper = likeMapper;
        this.likeRepository = likeRepository;
    }

    @Override
    public LikeDTO save(LikeDTO likeDTO) {
        log.debug("Request to save Like : {}", likeDTO);
        Like like = likeMapper.toEntity(likeDTO);
        like = likeRepository.save(like);
        return likeMapper.toDto(like);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete like : {}", id);
        likeRepository.deleteById(id);
    }

    @Override
    public void removeLike(Long userId, Long postId) throws Exception {
        log.debug("Request to delete like : : userId = {}, postId = {}", userId, postId);
        Like like = likeRepository.findByUserIdAndPostId(userId, postId).orElseThrow(() ->
                new Exception("Like not found"));
        this.delete(like.getId());
    }
}
