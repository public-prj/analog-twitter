package com.ras.analogtwitter.service;

import com.ras.analogtwitter.service.dto.UserDTO;

import java.util.List;
import java.util.Optional;

public interface UserService {

    UserDTO save(UserDTO userDTO);
    UserDTO update(UserDTO userDTO);
    void delete(Long id);

    List<UserDTO> findAll();

    Optional<UserDTO> findOne(Long id);


}
