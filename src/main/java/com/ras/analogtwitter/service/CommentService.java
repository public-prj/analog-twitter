package com.ras.analogtwitter.service;

import com.ras.analogtwitter.service.dto.CommentDTO;

import java.util.List;
import java.util.Optional;

public interface CommentService {
    CommentDTO save(CommentDTO commentDTO);
    CommentDTO update(CommentDTO commentDTO);
    void delete(Long id);

    List<CommentDTO> findAll();

    Optional<CommentDTO> findOne(Long id);

    List<CommentDTO> getCommentsByPostId(Long postId);
}
