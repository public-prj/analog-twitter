package com.ras.analogtwitter.service.mapper;

import com.ras.analogtwitter.model.Subscription;
import com.ras.analogtwitter.service.dto.SubscriptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface SubscriptionMapper {

    SubscriptionMapper instance = Mappers.getMapper(SubscriptionMapper.class);

    @Mapping(source = "subscriber.id", target = "subscriberId")
    @Mapping(source = "subscribee.id", target = "subscribeeId")
    SubscriptionDTO toDto(Subscription subscription);

    @Mapping(source = "subscriberId", target = "subscriber")
    @Mapping(source = "subscribeeId", target = "subscribee")
    Subscription toEntity(SubscriptionDTO subscriptionDTO);

    default Subscription fromId(Long id) {
        if (id == null) {
            return null;
        }
        Subscription subscription = new Subscription();
        subscription.setId(id);
        return subscription;
    }
}
