package com.ras.analogtwitter.service.mapper;

import com.ras.analogtwitter.model.Comment;
import com.ras.analogtwitter.model.Like;
import com.ras.analogtwitter.service.dto.LikeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {PostMapper.class, UserMapper.class})
public interface LikeMapper {
    LikeMapper instance = Mappers.getMapper(LikeMapper.class);

    @Mapping(source = "post.id", target = "postId")
    @Mapping(source = "user.id", target = "userId")
    LikeDTO toDto(Like like);

    @Mapping(source = "postId", target = "post")
    @Mapping(source = "userId", target = "user")
    Like toEntity(LikeDTO likeDTO);
    default Like fromId(Long id) {
        if (id == null) {
            return null;
        }
        Like like = new Like();
        like.setId(id);
        return like;
    }
}
