package com.ras.analogtwitter.service.mapper;

import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.model.User;
import com.ras.analogtwitter.service.dto.PostDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface PostMapper {
    PostMapper instance = Mappers.getMapper(PostMapper.class);

    @Mapping(source = "user.id", target = "userId")
    PostDTO toDto(Post post);

    @Mapping(source = "userId", target = "user")
    Post toEntity(PostDTO postDTO);

    default Post fromId(Long id) {
        if (id == null) {
            return null;
        }
        Post post = new Post();
        post.setId(id);
        return post;
    }
}
