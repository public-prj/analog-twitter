package com.ras.analogtwitter.service;

import com.ras.analogtwitter.service.dto.PostDTO;

import java.util.List;
import java.util.Optional;

public interface PostService {
    PostDTO save(PostDTO postDTO);
    PostDTO update(PostDTO postDTO);
    void delete(Long id);

    List<PostDTO> findAll();

    Optional<PostDTO> findOne(Long id);
}
