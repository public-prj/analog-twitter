package com.ras.analogtwitter.service;

import com.ras.analogtwitter.service.dto.LikeDTO;

public interface LikeService {
    LikeDTO save(LikeDTO likeDTO);

    void delete(Long id);

    void removeLike(Long userId, Long postId) throws Exception;
}
