package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.service.PostService;
import com.ras.analogtwitter.service.dto.PostDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class PostController {
    private final Logger log = LoggerFactory.getLogger(PostController.class);

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/posts")
    public ResponseEntity<PostDTO> createPost(@RequestBody final PostDTO postDTO) throws URISyntaxException {
        log.debug("REST request to Login : {}", postDTO.toString());
        PostDTO result = postService.save(postDTO);
        return ResponseEntity.created(new URI("/api/posts/" + result.getId() )).body(result);
    }
    @PutMapping("/posts")
    public ResponseEntity<PostDTO> updateBrand(@Valid @RequestBody PostDTO postDTO) throws URISyntaxException {
        log.debug("REST request to update Post : {}", postDTO);
        PostDTO result = postService.save(postDTO);
        return ResponseEntity.ok()
                .body(result);
    }
    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Void> deletePost(@PathVariable Long id) {
        log.debug("REST request to delete Post : {}", id);
        postService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
