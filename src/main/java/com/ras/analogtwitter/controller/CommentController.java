package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.service.CommentService;
import com.ras.analogtwitter.service.dto.CommentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentController {
    private final Logger log = LoggerFactory.getLogger(CommentController.class);
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/comments")
    public ResponseEntity<CommentDTO> createPost(@RequestBody final CommentDTO commentDTO) throws URISyntaxException {
        log.debug("REST request to create Comment : {}", commentDTO.toString());
        CommentDTO result = commentService.save(commentDTO);
        return ResponseEntity.created(new URI("/api/comments/" + result.getId() )).body(result);
    }

    @GetMapping("comments/{postId}")
    public ResponseEntity<List<CommentDTO>> getPostComments(@PathVariable Long postId) {
        log.debug("REST request to get comments by postId: {}", postId);
        List<CommentDTO> commentDTOS = commentService.getCommentsByPostId(postId);
        return ResponseEntity.ok().body(commentDTOS);
    }
}
