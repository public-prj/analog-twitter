package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.service.CommentService;
import com.ras.analogtwitter.service.LikeService;
import com.ras.analogtwitter.service.dto.CommentDTO;
import com.ras.analogtwitter.service.dto.LikeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class LikeController {

    private final Logger log = LoggerFactory.getLogger(LikeController.class);
    private final LikeService likeService;

    public LikeController(LikeService likeService) {
        this.likeService = likeService;
    }

    @PostMapping("/likes")
    public ResponseEntity<LikeDTO> createLike(@RequestBody final LikeDTO likeDTO) throws URISyntaxException {
        log.debug("REST request to create Like : {}", likeDTO.toString());
        LikeDTO result = likeService.save(likeDTO);
        return ResponseEntity.created(new URI("/api/likes/" + result.getId() )).body(result);
    }


    @DeleteMapping("/likes/{userId}/{postId}")
    public ResponseEntity<Void> deleteLike(@PathVariable Long userId, @PathVariable Long postId) throws Exception {
        log.debug("REST request to delete like : userId = {}, postId = {}", userId, postId);
        likeService.removeLike(userId, postId);
        return ResponseEntity.noContent().build();
    }
}
