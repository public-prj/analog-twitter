package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.service.SubscriptionService;
import com.ras.analogtwitter.service.dto.PostDTO;
import com.ras.analogtwitter.service.dto.SubscriptionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class SubscriptionController {

    private final Logger log = LoggerFactory.getLogger(SubscriptionController.class);

    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping("/subscribe")
    public ResponseEntity<SubscriptionDTO> subscribe(@RequestBody final SubscriptionDTO subscriptionDTO) throws URISyntaxException {
        log.debug("REST request to subscribe : {}", subscriptionDTO.toString());
        // In real world, we should get subscriber id from spring security token,
        // but as we do not have token here, we get subscriberId from client
        SubscriptionDTO result = subscriptionService.save(subscriptionDTO);
        return ResponseEntity.created(new URI("/api/subscribe/" + result.getId() )).body(result);
    }

    @DeleteMapping("/unsubscribe")
    public ResponseEntity<SubscriptionDTO> unsubscribe(@RequestBody final SubscriptionDTO subscriptionDTO) throws Exception {
        log.debug("REST request to unsubscribe : {}", subscriptionDTO.toString());
        subscriptionService.unsubscribe(subscriptionDTO);
        return ResponseEntity.noContent().build();
    }
}
