package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.AnalogTwitterApplication;
import com.ras.analogtwitter.TestUtil;
import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.model.User;
import com.ras.analogtwitter.repository.PostRepository;
import com.ras.analogtwitter.repository.UserRepository;
import com.ras.analogtwitter.service.dto.PostDTO;
import com.ras.analogtwitter.service.mapper.PostMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AnalogTwitterApplication.class)
@AutoConfigureMockMvc
public class PostControllerTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String DEFAULT_CONTENT = "BBBBBBBBBB";
    private static final String UPDATED_TITLE = "UPDATED_AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "UPDATED_BBBBBBBBBB";
    @Autowired
    private MockMvc restAddressMockMvc;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private UserRepository userRepository;
    private PostDTO postDTO;

    public static PostDTO createEntity() {
        PostDTO postDTO = new PostDTO();
        postDTO.setTitle(DEFAULT_TITLE);
        postDTO.setContent(DEFAULT_CONTENT);
        return postDTO;
    }

    @BeforeEach
    public void initTest() {
        postDTO = createEntity();
    }

    @Test
    public void createPost() throws Exception {

        List<User> users = userRepository.findAll();
        postDTO.setUserId(users.get(0).getId());
        int databaseSizeBeforeCreate = postRepository.findAll().size();
        restAddressMockMvc.perform(post("/api/posts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(postDTO)))
                .andExpect(status().isCreated());

        // Validate the Address in the database
        List<Post> postList = postRepository.findAll();
        assertThat(postList).hasSize(databaseSizeBeforeCreate + 1);
        Post testPost = postList.get(postList.size() - 1);
        assertThat(testPost.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testPost.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testPost.getUser().getId()).isEqualTo(users.get(0).getId());
    }

    @Test
    @Transactional
    public void updatePost() throws Exception {
        List<Post> postDTOS = postRepository.findAll();

        if (postDTOS.isEmpty()) {
            createPost();
            postDTOS = postRepository.findAll();
        }

        int databaseSizeBeforeUpdate = postDTOS.size();

        // Update the post
        Post updatedPost = postRepository.findById(postDTOS.get(postDTOS.size() - 1).getId()).get();
        updatedPost.setTitle(UPDATED_TITLE);
        updatedPost.setContent(UPDATED_CONTENT);

        PostDTO postDTO = postMapper.toDto(updatedPost);

        restAddressMockMvc.perform(put("/api/posts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(postDTO)))
                .andExpect(status().isOk());

        // Validate the Address in the database
        List<Post> postList = postRepository.findAll();
        assertThat(postList).hasSize(databaseSizeBeforeUpdate);
        Post testPost = postList.get(postList.size() - 1);
        assertThat(testPost.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testPost.getContent()).isEqualTo(UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void deletePost() throws Exception {
        List<Post> postDTOS = postRepository.findAll();

        if (postDTOS.isEmpty()) {
            createPost();
            postDTOS = postRepository.findAll();
        }

        int databaseSizeBeforeDelete = postDTOS.size();

        // Delete the address
        restAddressMockMvc.perform(delete("/api/posts/{id}", postDTOS.get(0).getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Post> postsList = postRepository.findAll();
        assertThat(postsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
