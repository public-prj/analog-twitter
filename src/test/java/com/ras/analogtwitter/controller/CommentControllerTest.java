package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.AnalogTwitterApplication;
import com.ras.analogtwitter.TestUtil;
import com.ras.analogtwitter.model.Comment;
import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.model.User;
import com.ras.analogtwitter.repository.CommentRepository;
import com.ras.analogtwitter.repository.PostRepository;
import com.ras.analogtwitter.service.dto.CommentDTO;
import com.ras.analogtwitter.service.dto.PostDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(classes = AnalogTwitterApplication.class)
@AutoConfigureMockMvc
public class CommentControllerTest {

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";

    @Autowired
    private MockMvc restMockMvc;
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PostRepository postRepository;
    private CommentDTO commentDTO;

    private Long postId;
    public static CommentDTO createEntity() {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText(DEFAULT_TEXT);
        return commentDTO;
    }

    @BeforeEach
    public void initTest() {
        commentDTO = createEntity();
        List<Post> posts = postRepository.findAll();
        postId = posts.get(posts.size() - 1).getId();
        commentDTO.setPostId(postId);
    }

    @Test
    public void createComment() throws Exception {

        int databaseSizeBeforeCreate = commentRepository.findAll().size();
        restMockMvc.perform(post("/api/comments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(commentDTO)))
                .andExpect(status().isCreated());

        List<Comment> commentList = commentRepository.findAll();
        assertThat(commentList).hasSize(databaseSizeBeforeCreate + 1);
        Comment testComment = commentList.get(commentList.size() - 1);
        assertThat(testComment.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testComment.getPost().getId()).isEqualTo(postId);
    }

    @Test
    public void getPostComments() throws Exception {
        restMockMvc.perform(get("/api/comments/" + postId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
    }
}
