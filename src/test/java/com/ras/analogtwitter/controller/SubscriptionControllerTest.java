package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.AnalogTwitterApplication;
import com.ras.analogtwitter.TestUtil;
import com.ras.analogtwitter.model.Subscription;
import com.ras.analogtwitter.model.User;
import com.ras.analogtwitter.repository.SubscriptionRepository;
import com.ras.analogtwitter.repository.UserRepository;
import com.ras.analogtwitter.service.SubscriptionService;
import com.ras.analogtwitter.service.dto.PostDTO;
import com.ras.analogtwitter.service.dto.SubscriptionDTO;
import com.ras.analogtwitter.service.dto.UserDTO;
import com.ras.analogtwitter.service.mapper.SubscriptionMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AnalogTwitterApplication.class)
@AutoConfigureMockMvc
public class SubscriptionControllerTest {
    @Autowired
    private MockMvc restAddressMockMvc;

    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubscriptionMapper subscriptionMapper;
    private UserDTO subscriberDTO;
    private SubscriptionDTO subscriptionDTO;

    public static UserDTO createSubscriber() {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("Subscriber first name");
        userDTO.setLastName("Subscriber last name");
        userDTO.setUsername("Subscriber username");
        userDTO.setPassword("Subscriber Password");
        return userDTO;
    }

    @BeforeEach
    public void initTest() {
        subscriberDTO = createSubscriber();
    }

    @Test
    public void subscribe() throws Exception {
        int databaseSizeBeforeCreate = subscriptionRepository.findAll().size();
        createUser();
        List<User> userList = userRepository.findAll();
        subscriptionDTO = new SubscriptionDTO();
        subscriptionDTO.setSubscriberId(userList.get(userList.size() - 1).getId());
        subscriptionDTO.setSubscribeeId(userList.get(0).getId());

        restAddressMockMvc.perform(post("/api/subscribe")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(subscriptionDTO)))
                .andExpect(status().isCreated());

        // Validate the Address in the database
        List<Subscription> subscriptionList = subscriptionRepository.findAll();
        assertThat(subscriptionList).hasSize(databaseSizeBeforeCreate + 1);
        Subscription testSubscription = subscriptionList.get(subscriptionList.size() - 1);
        assertThat(testSubscription.getSubscriber().getId()).isEqualTo(userList.get(userList.size() - 1).getId());
        assertThat(testSubscription.getSubscribee().getId()).isEqualTo(userList.get(0).getId());

    }

    @Test
    public void unsubscribe() throws Exception {
        List<Subscription> subscriptionDTOS = subscriptionRepository.findAll();
        int databaseSizeBeforeDelete = subscriptionDTOS.size();

        restAddressMockMvc.perform(delete("/api/unsubscribe")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(subscriptionMapper.toDto(subscriptionDTOS.get(0)))))
                .andExpect(status().isNoContent());

        // Validate the Address in the database
        List<Subscription> subscriptionList = subscriptionRepository.findAll();
        assertThat(subscriptionList).hasSize(databaseSizeBeforeDelete - 1);

    }

    public void createUser() throws Exception {
        restAddressMockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(subscriberDTO)))
                .andExpect(status().isCreated());
    }
}
