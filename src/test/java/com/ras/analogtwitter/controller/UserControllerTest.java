package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.AnalogTwitterApplication;
import com.ras.analogtwitter.TestUtil;
import com.ras.analogtwitter.model.User;
import com.ras.analogtwitter.repository.UserRepository;
import com.ras.analogtwitter.service.dto.UserDTO;
import com.ras.analogtwitter.service.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = AnalogTwitterApplication.class)
@AutoConfigureMockMvc
//@WithMockUser
public class UserControllerTest {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String DEFAULT_LAST_NAME = "BBBBBBBBBB";
    private static final String DEFAULT_USER_NAME = "USERNAME";
    private static final String DEFAULT_PASSWORD = "PASSWORD";
    private static final String UPDATED_FIRST_NAME = "UPDATED_AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "UPDATED_BBBBBBBBBB";
    private static final String UPDATED_USER_NAME = "UPDATED_USERNAME";
    private static final String UPDATED_PASSWORD = "UPDATED_PASSWORD";
    @Autowired
    private MockMvc restAddressMockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    private UserDTO userDTO;

    public static UserDTO createEntity() {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(DEFAULT_FIRST_NAME);
        userDTO.setLastName(DEFAULT_LAST_NAME);
        userDTO.setUsername(DEFAULT_USER_NAME);
        userDTO.setPassword(DEFAULT_PASSWORD);
        return userDTO;
    }

    @BeforeEach
    public void initTest() {
        userDTO = createEntity();
    }

    @Test
    public void registerUser() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();
        restAddressMockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(userDTO)))
                .andExpect(status().isCreated());

        // Validate the Address in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeCreate + 1);
        User testUser = userList.get(userList.size() - 1);
        assertThat(testUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testUser.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testUser.getUsername()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testUser.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void updateUser() throws Exception {
        List<User> userDTOS = userRepository.findAll();

        if (userDTOS.isEmpty()) {
            registerUser();
            userDTOS = userRepository.findAll();
        }

        int databaseSizeBeforeUpdate = userDTOS.size();

        // Update the user
        User updatedUser = userRepository.findById(userDTOS.get(userDTOS.size() - 1).getId()).get();
        updatedUser.setFirstName(UPDATED_FIRST_NAME);
        updatedUser.setLastName(UPDATED_LAST_NAME);
        updatedUser.setUsername(UPDATED_USER_NAME);
        updatedUser.setPassword(UPDATED_PASSWORD);

        UserDTO userDTO = userMapper.toDto(updatedUser);

        restAddressMockMvc.perform(put("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(userDTO)))
                .andExpect(status().isOk());

        // Validate the Address in the database
        List<User> userList = userRepository.findAll();
        assertThat(userList).hasSize(databaseSizeBeforeUpdate);
        User testUser = userList.get(userList.size() - 1);
        assertThat(testUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testUser.getUsername()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testUser.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void deleteUser() throws Exception {
        List<User> userDTOS = userRepository.findAll();

        if (userDTOS.isEmpty()) {
            registerUser();
            userDTOS = userRepository.findAll();
        }

        int databaseSizeBeforeDelete = userDTOS.size();

        // Delete the address
        restAddressMockMvc.perform(delete("/api/users/{id}", userDTOS.get(0).getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<User> usersList = userRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
