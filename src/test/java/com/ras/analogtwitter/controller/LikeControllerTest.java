package com.ras.analogtwitter.controller;

import com.ras.analogtwitter.AnalogTwitterApplication;
import com.ras.analogtwitter.TestUtil;
import com.ras.analogtwitter.model.Comment;
import com.ras.analogtwitter.model.Like;
import com.ras.analogtwitter.model.Post;
import com.ras.analogtwitter.model.User;
import com.ras.analogtwitter.repository.LikeRepository;
import com.ras.analogtwitter.repository.PostRepository;
import com.ras.analogtwitter.repository.UserRepository;
import com.ras.analogtwitter.service.dto.LikeDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AnalogTwitterApplication.class)
@AutoConfigureMockMvc
public class LikeControllerTest {

    @Autowired
    private MockMvc restMockMvc;

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    private LikeDTO likeDTO;

    private Long postId;
    private Long userId;

    @BeforeEach
    public void initTest() {
        likeDTO = new LikeDTO();
        List<Post> posts = postRepository.findAll();
        postId = posts.get(posts.size() - 1).getId();
        //
        List<User> users = userRepository.findAll();
        userId = users.get(users.size() - 1).getId();
        //
        likeDTO.setPostId(postId);
        likeDTO.setUserId(userId);
    }

    @Test
    public void addLike() throws Exception {
        int databaseSizeBeforeCreate = likeRepository.findAll().size();
        restMockMvc.perform(post("/api/likes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(likeDTO)))
                .andExpect(status().isCreated());

        List<Like> commentList = likeRepository.findAll();
        assertThat(commentList).hasSize(databaseSizeBeforeCreate + 1);
        Like testLike = commentList.get(commentList.size() - 1);
        assertThat(testLike.getPost().getId()).isEqualTo(postId);
        assertThat(testLike.getUser().getId()).isEqualTo(userId);
    }

    @Test
    public void removeLike() throws Exception {
        int databaseSizeBeforeDelete = likeRepository.findAll().size();

        // Delete the address
        restMockMvc.perform(delete("/api/likes/{userId}/{postId}", userId, postId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Like> likesList = likeRepository.findAll();
        assertThat(likesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
